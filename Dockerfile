FROM ubuntu:20.04

# General setup
WORKDIR /workspace
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8
RUN echo 'America/Los_Angeles' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        tzdata && \
    rm -rf /var/lib/apt/lists/*

# h3d_tools deps
RUN apt update &&\
  apt install -yq --no-install-recommends cmake build-essential python3-dev \
    python3-pip

# Copy the source code into the container
COPY . h3d_tools

# Install the h3d_tools package
RUN cd h3d_tools && pip install .

# Stream some data (default ip=192.168.2.10)
CMD python3 h3d_tools/scripts/stream_listmode.py
