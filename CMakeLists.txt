# Super simple CMakeLists for h3d_tools, with C++ code in src/cpp and
# python code (and bindings.cpp) in src/h3d_tools.
cmake_minimum_required(VERSION 3.15)
project(h3d_tools VERSION 0.2.1)

# C++ options
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/build_options.cmake)

# pybind11/scikit_build
if(SKBUILD)
  # Scikit-Build does not add your site-packages to the search path
  # automatically, so we need to add it _or_ the pybind11 specific directory
  # here.
  execute_process(
    COMMAND "${PYTHON_EXECUTABLE}" -c
            "import pybind11; print(pybind11.get_cmake_dir())"
    OUTPUT_VARIABLE _tmp_dir
    OUTPUT_STRIP_TRAILING_WHITESPACE COMMAND_ECHO STDOUT)
  list(APPEND CMAKE_PREFIX_PATH "${_tmp_dir}")
endif()
# Now we can find pybind11
find_package(pybind11 CONFIG REQUIRED)

# h3d_tools C++ source code
set(CPP_SOURCE_DIR ${PROJECT_SOURCE_DIR}/src/cpp)
include_directories(${CPP_SOURCE_DIR})
file(GLOB CPP_SOURCES ${CPP_SOURCE_DIR}/*.cpp)

# flatbuffers submodule, so a local install is not needed
add_subdirectory(${PROJECT_SOURCE_DIR}/lib/flatbuffers
                 ${CMAKE_CURRENT_BINARY_DIR}/flatbuffers-build
                 EXCLUDE_FROM_ALL)

# Access in python via `import h3d_tools.h3d_tools_cpp`
# or "from .h3d_tools_cpp import ..." from src/h3d_tools,
# where the .so will be installed
pybind11_add_module(
  h3d_tools_cpp
  MODULE
  ${CPP_SOURCES}
  ${PROJECT_SOURCE_DIR}/src/h3d_tools/bindings.cpp)
target_link_libraries(h3d_tools_cpp PRIVATE flatbuffers)

install(TARGETS h3d_tools_cpp DESTINATION .)
