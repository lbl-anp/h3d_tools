# h3d\_tools scripts #

Below are high-level descriptions of what each script does. In general, they will
take a command-line argument for the detector ip (--ip).

* `check_status.py`: uses `H3DStatus` to check the bias status, temperature, and
  humidity via the HTTP interface.
* `plot_current_spectrum.py`: requests the current spectrum from the HTTP interface
  and plots it.
* `record_raw_listmode.py`: stream listmode data and record it, byte-for-byte, to
  a raw .bin file.
* `stream_listmode.py`: stream listmode data and plot the energy spectrum of the
  data coming in.
* `test_bef_bss.py`: run a 20 second measurement, then grab the .bef and .bss files
  over the HTTP interface.
* `test_repeater.py`: this was specifically for LBNL-ORNL work, to test the code
  that repeats/mirrors the listmode data stream on another IP/port.
