#!/usr/bin/env python
"""Download all H3D data products from the detector for the most recent
measurement and write them to the provided directory. These are downloaded
via the HTTP API.

Because the link to the detector might be slow (10 MBit for the H420), this
may take awhile.
"""
import argparse
import h3d_tools

# Handle args
parser = argparse.ArgumentParser(
    description="Download H3D data products from a detector system as .zip"
)
parser.add_argument(
    "--ip",
    help="IP address of the detector",
    default="192.168.2.10",
    type=str,
)
parser.add_argument(
    "--path",
    help="Directory to write the zipped file to",
    default="",
    type=str,
)
args = parser.parse_args()

# Grab the goods
detector = h3d_tools.H3DStatus(detector_ip=args.ip)
detector.write_h3d_data_products_zip(args.path)
