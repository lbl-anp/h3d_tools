#!/usr/bin/python
"""Continuously stream listmode data from an H3D detector using this
module. Plots the energy spectrum as packets are received."""
# Interactive doesn't work with the OSX backend, at least on 10.15
import platform

if platform.system().casefold() == "darwin":
    import matplotlib

    matplotlib.use("Tkagg")
import matplotlib.pyplot as plt
import numpy as np
from h3d_tools import H3DDAQ
import argparse

parser = argparse.ArgumentParser(
    description="Stream listmode data from the detector and plot the spectrum"
)
parser.add_argument(
    "--ip",
    help="Detector IP (default=192.168.2.10)",
    default="192.168.2.10",
    required=False,
    type=str,
)
args = parser.parse_args()

# Keep the GUI event loop goin
plt.ion()
# Plot setup
NBINS = 3000
fig, ax = plt.subplots(figsize=(9, 6))
EDGES = np.linspace(0, 3000, NBINS + 1)
CENTERS = np.linspace(0.5, 3000.5, NBINS)
COUNTS = np.zeros(NBINS)
(line1,) = ax.plot(CENTERS, COUNTS, "r-", drawstyle="steps-mid")
plt.xlabel("Energy (keV)")
plt.ylabel("Counts")

# Initialize the DAQ, starting a new measurement
daq = H3DDAQ(detector_ip=args.ip, verbose=True)
daq.connect_listmode()
daq.stop_acq()
daq.set_fixed_acq(enable=False)
daq.start_acq("TestStreamScriptAcq")

# Stream data continuously while plotting
ntotal = 0
while daq.listmode_connected:
    if not daq.receive_listmode():
        continue
    (g, c, m, s) = daq.get_event_data()
    if len(g) < 1:
        continue
    ntotal += len(g)
    # Just update the existing histogram
    nc, _ = np.histogram(g["e"], bins=EDGES)
    COUNTS += nc
    line1.set_ydata(COUNTS)
    line1.set_label(f"Counts={ntotal}")
    plt.legend()
    plt.ylim([0.00001, np.max(COUNTS) * 1.1])
    fig.canvas.draw()
    fig.canvas.flush_events()
    # No sleep, we want to loop as fast as the CPU will allow
