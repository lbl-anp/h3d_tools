#!/usr/bin/env python
"""Connect to the H420, stream listmode data, and calculate the deadtime based
on the info provided by H3D.

This value should be compared against the value reported in the H3D UI.

From Jason Jaworski:
    'You should be able to sum up the live times of all of the events from the'
    list-mode stream to get the overall livetime of the system during the time'
    period you are summing'
"""
from h3d_tools import H3DDAQ
import numpy as np
import time

# Initialize the DAQ, starting a new measurement
daq = H3DDAQ(detector_ip="192.168.2.10", repeat_raw=False, verbose=True)
daq.connect_listmode()
daq.stop_acq()
daq.set_fixed_acq(enable=False)
daq.start_acq("TestDeadtimeScriptAcq")

start = time.time()
livetime = 0.0
while daq.listmode_connected:
    if not daq.receive_listmode():
        continue
    (g, c, m, s) = daq.get_event_data()
    if len(g) < 1:
        continue
    if len(c) == 1:
        h420_sys = c["sys_sec"][0]
        h420_sys += c["sys_ns"][0] * 1e-9
        print(f"Systime Diff (local-h3d): {time.time() - h420_sys}")

    livetime += np.sum(g["lt"]) * 1e-8
    elapsed = time.time() - start
    print(f"{livetime=}")
    print(f"Avg Livetime %: {100. * livetime / elapsed}")
    print(f"Avg Deadtime %: {100. * (1. - (livetime / elapsed))}")

    time.sleep(0.5)
