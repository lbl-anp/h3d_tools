#!/usr/bin/env python
"""Get the current spectrum over HTTP and plot."""
from h3d_tools import H3DStatus
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(
    description="Get the current spectrum over HTTP and plot"
)
parser.add_argument(
    "--ip",
    help="Detector IP (default=192.168.2.10)",
    default="192.168.2.10",
    required=False,
    type=str,
)
args = parser.parse_args()

status = H3DStatus(detector_ip=args.ip)

spectrum = status.get_spectrum().split("\n")
# Just the last element I think
spectrum = np.array([s for s in spectrum if s != ""], dtype=int)

# Plot
nbins = len(spectrum)  # 8192
EDGES = np.linspace(0, nbins, nbins + 1, dtype=int)
plt.hist(EDGES[:-1], bins=EDGES, weights=spectrum)
print(f"Total counts = {np.sum(spectrum)}")
plt.xlim([0, 1400])
plt.xlabel("Energy (keV)")
plt.ylabel("Counts/keV")
plt.show()
