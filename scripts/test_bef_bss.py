#!/usr/bin/env python
"""This script tests the BEF/BSS file extraction functionality. There is no
explicit API call provided by H3D at the moment; instead, we have to use
regular HTTP requests, get the file lists, then download the data."""
from h3d_tools import H3DStatus
import argparse
import time

parser = argparse.ArgumentParser(
    description="Get the latest BEF/BSS files and print the bytes"
)
parser.add_argument(
    "--ip",
    help="Detector IP (default=192.168.2.10)",
    default="192.168.2.10",
    required=False,
    type=str,
)
args = parser.parse_args()

# Do a 20 second run (<10 sec gets dicey)
print("Starting a 20 second measurement, please wait...")
status = H3DStatus(detector_ip=args.ip)
status.stop_acq()
status.set_fixed_acq(True, duration=20)
status.start_acq()
nsleeps = 0
while True:
    time.sleep(1)
    nsleeps += 1
    print(f"Elapsed time: {nsleeps} sec...")
    if nsleeps == 20:
        break
# Acquisition must be stopped to get the BEF/BSS data
# We set a fixed acquisition, so no need to manually stop here
# We do, however, need to wait for the detector to finish writing these files
time.sleep(2)
PAD = "**********"
print(f"{PAD} BEF DATA {PAD}")
print(status.get_bef_bss("bef"))
print("\n")
print(f"{PAD} BSS DATA {PAD}")
print(status.get_bef_bss("bss"))
