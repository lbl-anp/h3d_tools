#!/usr/bin/env python
"""Confirm that the bytes repeated by H3DDAQ can be unpacked with flatbuffers.
For this script to work, the DAQ must be running and repeating on
192.168.9.1, port 4420, and the local network must be configured to accommodate
this."""
import socket
import select
import time
from .h3d_api_doc.H3DData import H3DPacket


def main():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = "192.168.9.1"
    port = 4420
    client.connect((ip, port))
    print(f"Connected to {ip} on port {port}")

    # Essentially a duplication of the code in H3DDAQ for receiving data
    while True:
        to_read = [client]
        read_ready = select.select(to_read, [], [], 1)  # timeout = 1

        packet_size = 0
        buf = b""
        if read_ready:
            try:
                psize_bytes = client.recv(4)
                packet_size = int.from_bytes(
                    psize_bytes, byteorder="little", signed=True
                )
                # Packets may be split across sends; loop until we get the
                # whole thing
                while len(buf) < packet_size:
                    buf += client.recv(packet_size - len(buf))
            except socket.timeout:
                # Timeouts don't necessarily indicate a bad connection
                print("Exception: Timed out")
                client.close()
                return
            except OSError as ex:
                print(f"Exception: {ex}")
                client.close()
                return
        else:
            print("There were no data")
            time.sleep(1)
            continue

        packet = None
        if packet_size > 0:
            # Integrity check; this should always be true! If it happens,
            # though, try to skip to the next one
            if packet_size != len(buf):
                print(
                    f"ERROR! expected packet_size({packet_size}) != "
                    f"received buffer_size({len(buf)})!\nSkipping packet"
                )
                return
            print(f"{packet_size} bytes received")
            packet = H3DPacket.H3DPacket.GetRootAsH3DPacket(buf, 0)
            print(f"Packet had {packet.GammaeventsLength()} gamma events")
            print(f"GammaEvent0: {packet.Gammaevents(0).Interactions(0)}")
            print(f"GammaEvent0,X:{packet.Gammaevents(0).Interactions(0).X()}")
            time.sleep(1)
            continue
        else:
            # recv() returned 0 while read_ready was true, we're in a bad state
            # client.close()
            print("Packet size was 0")
            return


if __name__ == "__main__":
    main()
