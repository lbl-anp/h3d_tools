#!/usr/bin/env python
"""For debugging purposes: start a measurement and record the raw bytes from the
listmode stream to disk. Additionally, get the entire contents of the
Measurements directory (via HTTP) and write that as well.

These are data we can provide to H3D for troubleshooting purposes."""
import h3d_tools as h3t
import time
import argparse

parser = argparse.ArgumentParser(
    description="Record the listmode raw byte stream directly to disk"
)
parser.add_argument(
    "--ip",
    help="Detector IP (default=192.168.2.10)",
    default="192.168.2.10",
    required=False,
    type=str,
)
parser.add_argument(
    "--duration",
    help="How many seconds to acquire data (default=60 sec)",
    default=60.0,
    required=False,
    type=float,
)
parser.add_argument(
    "--path",
    help="Directory path to write data files (default=current dir)",
    default=".",
    required=False,
    type=str,
)
args = parser.parse_args()

daq = h3t.H3DDAQ(detector_ip=args.ip)
# Make sure we are starting a fresh measurement
daq.stop_acq()
daq.set_fixed_acq(enable=True, duration=args.duration)
daq.start_acq("DebugMeasurement")
daq.connect_listmode()
start_t = time.time()

# Start streaming
ntot = 0
# Print every second
print_timer = 1  # seconds
raw_stream = b""
while daq.listmode_connected:
    if not daq.receive_listmode():
        continue
    (g, c, m, s) = daq.get_event_data()
    if len(g) < 1:
        continue
    # Keep adding to the buffer of raw bytes
    raw_stream += daq.get_last_buffer()

    elapsed = time.time() - start_t
    if elapsed > print_timer:
        print(f"Got {len(raw_stream)} bytes so far. Time elapsed: {elapsed} sec")
        print_timer += 1.0
    ntot += len(g)
    if not daq.is_acquiring():
        print("DAQ measurement was stopped")
        break

args.path.rstrip("/")
print(f"Finished acquiring {ntot} events. Writing to {args.path}/raw.bin...")
with open(args.path + "/raw.bin", "wb") as f:
    f.write(raw_stream)

# Write data products
print(
    "Grabbing measurement data from the detector and writing to "
    f"{args.path}/AllEvents.bef and {args.path}/AllEvents.bss"
)
daq.status.write_h3d_data_products(args.path)
