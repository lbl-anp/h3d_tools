# H3D DAQ Examples #

This is from their API documentation, which can be found
[here](https://drive.google.com/drive/folders/1chBW0QaPsLqmtI4A2S6gE86VPE7LEguU)

Copy-pasted C++ and python code, lightly modified.

In both cases, the codes connect to the detector via socket, then stream the
listmode data, unpacking it using `flatbuffers`. See
[the main README](../../README.md) for more info.

## Flatbuffers ##

Flatbuffers was run on the scheme provided by H3D.

C++: `flatc --cpp h3ddata_schema.fbs` --> `h3ddata_schema_generated.h`

Python: `flatc --python h3ddata_schema.fbs` --> `H3DData` directory with .py
definitions

## C++ Code ##

1. `mkdir build && cd build`
1. `cmake ..`
1. `make -j`
1. `./test` to run

## Python Code ##

`python daq.py` to run
