/*
Authors: Hao Yang and Jason Jaworski
Date: 12/30/2020
Organization: H3D, Inc.
listModeParser.cpp -- a stream socket client demo
This example c++ function reads data from the list-mode stream published by an
H3D detector system. The function depends on standard c++ libraries and the
Google Flatbuffers library. The user is responsible for generating the
H3DData_generated.h file using the flatc compiler.

Modified by Micah Folsom, Nov 15, 2021
*/
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <ctime>
#include <iostream>
#include <vector>

#include "h3ddata_schema_generated.h"

#define PORT "11503"
#define IP "192.168.3.10"

using namespace std;

int main(int argc, char* argv[]) {
  int sockfd;
  struct addrinfo hints, *servinfo;
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  getaddrinfo(IP, PORT, &hints, &servinfo);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) < 0) {
    printf("Connection failed.\n");
    return 1;
  }
  freeaddrinfo(servinfo);  // all done with this structure
  int nBufferSize(0);
  std::vector<char> Buffer;

  while (true) {
    int num2recv(0);
    recv(sockfd, &nBufferSize, sizeof(nBufferSize), 0);
    Buffer.resize(nBufferSize);
    char* p(Buffer.data());
    while (num2recv != nBufferSize) {
      num2recv += recv(sockfd, p + num2recv, nBufferSize - num2recv, 0);
    }
    auto H3DDataPacket = H3DData::GetH3DPacket(p);

    auto events = H3DDataPacket->gammaevents();
    if (events) {
      // Checking the pointer isn't enough...
      if (events->size() > 0) {
        cout << "GammaEvents: chip, x, y, z, energy" << endl;
      }
      for (unsigned i = 0; i < events->size(); i++) {
        auto interactions = events->Get(i)->interactions();
        for (unsigned j = 0; j < interactions->size(); j++) {
          auto x = interactions->Get(j)->x();
          auto y = interactions->Get(j)->y();
          auto z = interactions->Get(j)->z();
          auto energy = interactions->Get(j)->energy();
          auto chip = interactions->Get(j)->chip();
          printf("%u %d %d %d %d\n", (int)chip, x, y, z, energy);
        }
        auto timestamp = events->Get(i)->timestamp();
        auto livetime = events->Get(i)->livetime();
        // printf("%lu %u\n", timestamp, livetime);
      }
    }

    auto syncevents = H3DDataPacket->syncevents();
    if (syncevents) {
      for (unsigned i = 0; i < syncevents->size(); i++) {
        auto index = syncevents->Get(i)->index();
        auto timestamp = syncevents->Get(i)->timestamp();
      }
    }

    auto clockevents = H3DDataPacket->clockevents();
    if (clockevents) {
      // Checking the pointer isn't enough...
      if (clockevents->size() > 0) {
        cout << "ClockEvents: clock_sec, clock_ns, fpga_ts" << endl;
      }
      for (unsigned i = 0; i < clockevents->size(); i++) {
        auto clockseconds = clockevents->Get(i)->clockseconds();
        auto clocknanoseconds = clockevents->Get(i)->clocknanoseconds();
        auto timestamp = clockevents->Get(i)->timestamp();
        printf("%d %d %lu\n", clockseconds, clocknanoseconds, timestamp);
      }
    }

    auto maskevents = H3DDataPacket->maskevents();
    if (maskevents) {
      for (unsigned i = 0; i < maskevents->size(); i++) {
        auto rotating = maskevents->Get(i)->rotating();
        auto quadrant = maskevents->Get(i)->quadrant();
        auto realtime = maskevents->Get(i)->realtime();
        auto livetime = maskevents->Get(i)->livetime();
        auto timestamp = maskevents->Get(i)->timestamp();
        printf("%d\n", (int)rotating);
      }
    }
  }
  return 0;
}
