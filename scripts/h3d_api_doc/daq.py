# Author: Jason Jaworski
# Date: 12/30/2020
# Organization: H3D, Inc.
# listModeParser.py -- a stream socket client demo
# This example python script reads data from the list-mode stream published by
# an H3D detector system.
# The script depends on the Google Flatbuffers library.
# The user is responsible for generating the H3DData python files using the
# flatc compiler.
#
# Modified by Micah Folsom, Dec 8, 2021
from H3DData import H3DPacket
import socket
import time
import numpy as np

# Pre-allocate a big numpy array to hold the data so we're not constantly resizing
# a list
REALLY_BIG = 100000
data = np.zeros(
    shape=(REALLY_BIG,),
    dtype=[
        ("energy", np.uint32),
        ("x", np.uint16),
        ("y", np.uint16),
        ("z", np.uint16),
    ],
)


def daq():
    HOST = "192.168.2.10"
    PORT = 11503
    ADDR = (HOST, PORT)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(ADDR)

    while True:
        loop_start = time.perf_counter_ns() * 1e-9

        packetSize = int.from_bytes(client.recv(4), byteorder="little", signed=True)
        buffer = client.recv(packetSize)
        while len(buffer) < packetSize:
            buffer += client.recv(packetSize - len(buffer))
        packetIn = H3DPacket.H3DPacket.GetRootAsH3DPacket(buffer, 0)

        print("Gamma events:", packetIn.GammaeventsLength())
        nint = 0
        # We *must* loop to get the data. flatbuffers only provides AsNumpy()
        # functions for vectors of scalars, but we only have vectors of objects
        # There *may* be a way if we bypass the flatbuffers interface and deal
        # directly with the buffer, but that defeats the point of using it in the
        # first place
        if packetIn.GammaeventsLength() > 0:
            for i in range(packetIn.GammaeventsLength()):
                for j in range(packetIn.Gammaevents(i).InteractionsLength()):
                    data["energy"][nint] = (
                        packetIn.Gammaevents(i).Interactions(j).Energy()
                    )
                    data["x"][nint] = packetIn.Gammaevents(i).Interactions(j).X()
                    data["y"][nint] = packetIn.Gammaevents(i).Interactions(j).Y()
                    data["z"][nint] = packetIn.Gammaevents(i).Interactions(j).Z()
                    nint += 1
        print(f"Got {nint} interactions")
        # print(data['energy'][:100])

        print("Clock events:", packetIn.ClockeventsLength())
        if packetIn.ClockeventsLength() > 0:
            """
            print("Clock Seconds:", packetIn.Clockevents(0).Clockseconds())
            print("Clock Nanoseconds:", packetIn.Clockevents(0).Clocknanoseconds())
            print("Timestamp:", packetIn.Clockevents(0).Timestamp())
            print("Sync events: ", packetIn.SynceventsLength())
            """
        if packetIn.SynceventsLength() > 0:
            """
            print("Index:", packetIn.Syncevents(0).Index())
            print("Timestamp:", packetIn.Syncevents(0).Timestamp())
            print("Mask events: ", packetIn.MaskeventsLength())
            """
        if packetIn.MaskeventsLength() > 0:
            """
            print("Rotating:", packetIn.Maskevents(0).Rotating())
            print("Quadrant:", packetIn.Maskevents(0).Quadrant())
            print("Realtime:", packetIn.Maskevents(0).Realtime())
            print("Livetime:", packetIn.Maskevents(0).Livetime())
            print("Timestamp:", packetIn.Maskevents(0).Timestamp())
            """
        now = time.perf_counter_ns() * 1e-9
        print(f"Loop took {now - loop_start} sec")


if __name__ == "__main__":
    daq()
