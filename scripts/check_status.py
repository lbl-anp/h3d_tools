#!/usr/bin/python
"""Grab all available status information from the detector."""
from h3d_tools import H3DStatus
import argparse

parser = argparse.ArgumentParser(
    description="Get and print the detector status information"
)
parser.add_argument(
    "--ip",
    help="Detector IP (default=192.168.2.10)",
    default="192.168.2.10",
    required=False,
    type=str,
)
args = parser.parse_args()

status = H3DStatus(detector_ip=args.ip)

# Set a fixed run time
# status.set_fixed_acq(True, 60)
# Run forever
status.stop_acq()
status.set_fixed_acq(enable=False)
status.start_acq()

# Get the energy spectrum
# print(status.get_spectrum())

# Temperature status
print(f"Temperature status: {status.get_temp_status()}")

# Relative humidity delta
print(f"Humidity status: {status.get_humidity_status()}")

# Bias status
print(f"Biased?: {status.is_biased()}")
