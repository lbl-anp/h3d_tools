# `h3d_tools` Binaries #

## BEF/BSS Converters ##

`BEFConverter.exe` and `BSSConverter.exe` are binaries provided by H3D to
convert the .bss (mask data) and .bef (gamma event data) files to readable data
formats. The .bef file is converted to .bin, and the .bss file is converted to
.txt.

Original executable names:

* `BSSConverter.exe`: `ElseBinaryFileConverterv2_protected.exe`
* `BEFConverter.exe`: `JeanneElseLenoreFaithBinaryFileConverter_protected.exe`

### Usage ###

These binaries can be run using [WINE](https://wiki.winehq.org/Ubuntu) on
Ubuntu. Install WINE via `apt get`, then use `wine BSSConverter.exe` and
`wine BEFConverter.exe` to launch these command-line converter tools.

### Output Formats ###

The mask data (.bss) is output to a plain text file, with 3 columns: timestamp,
whether or not the mask is rotating, and which quadrant it's in.

The gamma data (.bef) is output to binary. There was some out of date
documentation describing the format, so I had to reverse-engineer some of it.
The format is as follows:

#### Header ####

1. Version (4 bytes, uint)
1. Number of events (8 bytes, uint)
1. Number of interactions (8 bytes, uint)
1. Not sure (4 bytes, uint)

There are 24 bytes total of header data. Past this, it's all event data.

#### Event Data ####

1. Number of interactions in this event (4 bytes, uint)
1. Energy in keV (4 bytes, float)
1. X position in mm (4 bytes, float)
1. Y position in mm (4 bytes, float)
1. Z position in mm (4 bytes, float)
1. Timestamp in sec (time since start) (4 bytes, float)
1. Livetime since last event in usec (4 bytes, float)

The binary data can be read into python numpy arrays very easily:

```python
import numpy as np
with open("GammaEvents.bin", "rb") as f:
  data = f.read()

dtype = [
    ("num_interaction", np.uint32),
    ("energy", np.float32),
    ("x", np.float32),
    ("y", np.float32),
    ("z", np.float32),
    ("ts", np.float32),
    ("lt", np.float32),
]
data_arr = np.frombuffer(data[24:], dtype=dtype)
```

### Validation ###

Comparison against .bag data was performed
[here](https://gitlab.com/lbl-anp/quantitative_safeguards/-/tree/main/bef_bss_validation)

### Note ###

`BEFConverter.exe` seems to understand how to convert `.bss` files, but not
vice versa. The only issue is that it only knows how to output one file:
`AllEvents.bin` to the local directory. So you have to run it on the `.bss`
first, rename it to something else (such as `AllEvents.txt`), then run
`BEFConverter.exe` again on the `.bef` file to get the actual binary with
listmode data.
