GammaEvent
**************************

.. doxygenstruct:: h3d_tools::GammaEvent
  :project: h3d_tools_cpp
  :members: x, y, z, e, lt, ts
