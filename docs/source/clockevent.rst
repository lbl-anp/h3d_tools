ClockEvent
**************************

.. doxygenstruct:: h3d_tools::ClockEvent
  :project: h3d_tools_cpp
  :members: sys_sec, sys_ns, ts
