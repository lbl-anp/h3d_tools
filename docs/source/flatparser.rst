Flatparser
**************************

.. doxygenfunction:: unpack_gamma_events
  :project: h3d_tools_cpp
.. doxygenfunction:: unpack_clock_events
  :project: h3d_tools_cpp
.. doxygenfunction:: unpack_mask_events
  :project: h3d_tools_cpp
.. doxygenfunction:: unpack_sync_events
  :project: h3d_tools_cpp
