MaskEvent
**************************

.. doxygenstruct:: h3d_tools::MaskEvent
  :project: h3d_tools_cpp
  :members: rotating, qudrant, rt, lt, ts
