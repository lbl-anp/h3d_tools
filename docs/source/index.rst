.. prism_tools documentation master file, created by
   sphinx-quickstart on Mon Oct  4 11:19:31 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to h3d_tools's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   flatparser
   gammaevent
   clockevent
   maskevent
   syncevent
   h3ddaq
   h3dstatus


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
