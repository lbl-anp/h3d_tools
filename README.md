# H3D Tools #

h3d\_tools contains tools for communicating with the 2021 API provided with the
H3D detector systems.

## Table of Contents ##

[[_TOC_]]

## Documentation ##

`sphinx` documentation can be found [here](https://lbl-anp.gitlab.io/h3d_tools/)

## Installation ##

Make sure to clone this repository with `git clone --recursive` so that the
`flatbuffers` submodule codes is downloaded.

Since `flatbuffers` is in a submodule, all that are needed locally
is `python3` a C++ compiler. On Ubuntu: `sudo apt install build-essential
python3-dev python3-pip`. On macOS, install the Xcode command-line tools.

This is just a standard python module that can be installed with `pip install
[--user] .` from the base directory of this repository. `pybind11` is used to
create python bindings for the C++ code, which is compiled into a wheel.
Editable installs are questionable, just note that only the python code is live -
you will need to rerun `pip install` to recompile changes in C++ code.

## Supported Systems ##

H3D uses the same software across many systems, so in principle, this code should
work with most of them. It has been tested with:

1. `H420`: <https://h3dgamma.com/H420Specs.pdf?>
1. `M400`: <https://h3dgamma.com/M400Specs.pdf?>

The `system_name` is an `__init__` argument for `H3DDAQ`, so system-specific
implementations can easily be added to the code.

All systems are connected over ethernet. The M400 comes with two usb-c
connectors. One needs to be connected to a usb-c hub or usb plug solely for
getting 5V of power. The other one needs to be connected to a usb-c to
ethernet dongle (inverse to what we often do on NUCs), and then connect
with ethernet to the host so that it can talk to them. The cables are not
interchangable, so be sure to connect them correctly.

The IP addresses for the two systems are typically:

1. `H420`: `192.168.2.10`
1. `M400`: `192.168.3.10`

There is no way of configuring them

## Usage ##

The `H3DDAQ` object is the user-facing DAQ interface object. The DAQ is split
into 2 components: listmode data acquisition and HTTP detector control. The
`H3DStatus` object handles the HTTP communication, and exists as an attribute
to `H3DDAQ` (`.status`) - so one should use `H3DDAQ` as the primary interface
to collect data from an H3D detector.

A number of parameters, such as the detector IP and port, can be specified when
creating the `H3DDAQ` in its `__init__()`. Please check the docstrings for more
info on which parameters are available.

At a high level, the user is responsible for calling functions on `H3DDAQ` to:

1. Connect to the detector
1. Start and stop measurements (note: listmode data is always streaming, even
  if no measurement is underway) or set a fixed acquisition time
1. Periodically asking `H3DDAQ` for event data, at a high enough frequency to
  keep up with the data rate

This last item is important - the H420 publishes at ~1 Hz, and the M400
publishes at ~5-10 Hz at background data rates, so the user will want to poll
for data more frequently than this. Empirically, testing at around 100k cps,
the M400 seems to publish at most at about 17 Hz. **It is recommended to call
`H3DDAQ.receive_listmode()` then `H3DDAQ.get_event_data()`, in that order, at
a rate of at least 30 Hz to guarantee that all of the data is captured.**

An example script that loops and reads out data might look like this:

```python
daq = H3DDAQ(detector_ip="192.168.2.10", repeat_raw=False, verbose=True)
daq.connect_listmode()
# Make sure the detector is stopped, since on the H420 it can be started with a
# physical button press
daq.stop_acq()
# Fixed acquisition is false, so it'll just run until we stop it, otherwise
# we may provide a duration
daq.set_fixed_acq(enable=False)
daq.start_acq("MyMeasurementName")
# Loop as fast as possible. Be wary of sleep() since we may not keep up
# with the data rate - 30 Hz (`sleep(0.0333)`) should be fast enough
while daq.listmode_connected():
  (gamma, clock, mask, sync) = daq.get_event_data()
  # gamma is now a numpy.ndarray with 'e', 'x', 'ts', and other fields
  # containing the radiation data.
  # clock, mask, and sync are also structured numpy.ndarrays
  # This will print an array of interaction energies in keV
  print(gamma['e'])
```

When browsing the files on the detector system, ie. when getting the .bef or
.bss data, the measurement name (`MyMeasurementName`) will be used in the
generation of the folder name for these data products.

For more examples using the `H3DDAQ` object, see the `scripts/` directory.

### Scripts ###

See the [scripts README](scripts/README.md) for descriptions of the python
scripts.

## Data Formats/Units ##

Different from H3D, we reverse the x-coordinate to maintain a right-handed
coordinate system.

* X: Up (H3D assumes X is down)
  * We negate X to transform into a right-handed coordinate system
* Y: Right
* Z: Forward
* X,Y,Z units in meters (H3D gives us micrometers)
* Energy: keV (H3D gives us eV)
* Gamma event livetime: 10s of ns
* Mask livetime/realtime: msec
* FPGA timestamps: 10s of ns

## Dependencies ##

A C++ compiler (g++ or clang) is needed on your local system

Python3.6-3.10 installed locally or via a conda environment

All other dependencies can be downloaded via `pip` (see: requirements.txt)
`pip install -r requirements.txt`

`flatbuffers` is in a submodule, explained in the next few sections.

### `pybind11` ###

The `pip` version of [`pybind11`](https://pybind11.readthedocs.io/en/stable/)
is used to build this package and it will be automatically installed when one
attempts to install the package because of the config in `pyproject.toml`
listing build dependencies.

`pybind11` uses `CMakeLists.txt` in the base of the repo to do its magic, along
with bindings defined in `src/h3d_tools/bindings.cpp`. `src/cpp/CMakeLists.txt`
is available for compiling the C++ code independently of the python module.

Python code should go into `src/h3d_tools` and C++ code should go into `src/cpp`.
C++ files that are added will be automatically detected and compiled, but you
must add bindings to `src/h3d_tools/bindings.cpp` to use it in python.

### `flatbuffers` ###

[`flatbuffers`](https://github.com/google/flatbuffers) is in `lib/` as a
submodule. This code is built along with the package so no local dependencies
are needed.

It's important that the version of `flatbuffers.h` and the version of the header
generated by the `flatc` command be the same. That is, one should compile the
`flatbuffers` submodule, and use the `flatc` binary that it creates to generate
the schema from the `.fbs` file. That way, `flatbuffers.h` and the schema that
are generated match. This is already done in this repo.

If the `flatbuffers` submodule is updated to a new commit hash, the H3D data
headers should be regenerated with the `flatc -c h3ddata_schema.fbs` command.

## H3D API (Jan. 2021) ##

The H3D API released in January, 2021, allows for three modes of
communication with the detector system. See the API documentation for more
details.

1. WebREST (HTTP)
1. XML (N42.42 specification)
1. Binary (using [FlatBuffers](https://google.github.io/flatbuffers/))

Methods 1 and 2 are bidirectional and can be used to control
measurements, whereas 3 is read only. The XML channel sends high level
data, such as isotope IDs, spectra, camera images, and GPS coordinates.
The binary channel contains listmode gamma events, as well as periodic
clock, mask, and sync events. Listmode data are sent continuously, whether or
not a measurement has been started. It is streamed even when the mask is
rotating.

Note that the H420 *MUST* have a hard drive (USB drive) plugged in, or else
listmode data *will not stream* (connection refused).

It is **NOT** recommended to use the XML (N42) pipe. It's not officially
supported and it sounds like H3D only uses it for internal debugging, with no
intent to make it generally available/documented to users. It's also a pain to
deal with and parse! This DAQ code does not support this communication channel.

The H420 system has three states. I believe this applies to H3D systems in
general, but this hasn't been confirmed explicitly.

1. Running: a measurement is currently in progress
1. Stopped: not currently acquiring, and not in Search mode (stationary)
1. Search: the accelerometer detects that you move the system while Stopped,
   and maintains a 60 second buffer; it behaves like Running, but only
   using these buffered data.

### Listmode Data Structures ###

The `flatbuffers` schema can be found in the
`scripts/h3d_api_doc/h3ddata_schema.fbs` file, which was provided by H3D.
Previously, we used the python interface, however *it is not performant enough*.
We found that at count rates exceding ~4k cps, the event processing could not
keep up with the data rate due to python `flatbuffers` being very slow. What
this count rate limit is depends entirely on the performance of the computer
acquiring data and may not be a bottleneck in some scenarios, but either way,
the python `flatbuffers` interface is not recommended.

Instead, the `flatbuffers` unpacking is performed in a compiled C++ function
that is bound to python using `pybind11`. The buffer is passed in and the
function returns structured `numpy.ndarray`s with the data. The C++ header was
generated using the `flatc --cpp h3ddata_schema.fbs` command.

## ORNL Custom Listmode Repeater ##

The `H3DDAQ` object will optionally repeat the raw bytes received from the H420
on a specifiable IP and port. This was a feature requested by ORNL so they could
access the gamma data in real time with their software while also running ours.
It is a byte-for-byte reproduction of the listmode stream and it is disabled
by default.

## BEF/BSS File Extraction ##

H3D systems record listmode and (if available) mask data in priority data
formats, stored as .bef and .bss. These can be accessed using specific HTTP
calls, which are documented
[here](https://drive.google.com/drive/folders/1-Sk8Wz27mzWfiZXtdkLJCkBxxVA97Wki)
in `H3DScansRestAPI.pdf`. If you don't have access to this document, ask H3D
for the "Scans REST API" documentation.

Otherwise, one may navigate to `<detector ip>/scans` in a browser to see
the full list of available measurement directories, in which the .bef and .bss
files reside. Additionally, for the H420, these files are on the removable thumb
drives used to operate the system.

## Dockerfile ##

A `Dockerfile` is included to create a Ubuntu 20.04 container with the `h3d_tools`
package installed. Run `docker build --rm -t h3d_daq .` to build.

## Copyright ##

H3D Data Acquisition Software (H3DDAQ) Copyright (c) 2022, The Regents of the
University of California, through Lawrence Berkeley National Laboratory (subject
to receipt of any required approvals from the U.S. Dept. of Energy). All rights
reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.

NOTICE. This Software was developed under funding from the U.S. Department of
Energy and the U.S. Government consequently retains certain rights.  As such,
the U.S. Government has been granted for itself and others acting on its behalf
a paid-up, nonexclusive, irrevocable, worldwide license in the Software to
reproduce, distribute copies to the public, prepare derivative  works, and
perform publicly and display publicly, and to permit others to do so.

## License ##

H3D Data Acquisition Software (H3DDAQ) Copyright (c) 2022, The Regents of the
University of California, through Lawrence Berkeley National Laboratory (subject
to receipt of any required approvals from the U.S. Dept. of Energy). All rights
reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

(1) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

(2) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

(3) Neither the name of the University of California, Lawrence Berkeley National
Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used
to endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You are under no obligation whatsoever to provide any bug fixes, patches, or
upgrades to the features, functionality or performance of the source code
("Enhancements") to anyone; however, if you choose to make your Enhancements
available either publicly, or directly to Lawrence Berkeley National Laboratory,
without imposing a separate written license agreement for such Enhancements,
then you hereby grant the following license: a non-exclusive, royalty-free
perpetual license to install, use, modify, prepare derivative works, incorporate
into other computer software, distribute, and sublicense such enhancements or
derivative works thereof, in binary and source code form.
