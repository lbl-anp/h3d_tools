#include "flatparser.hpp"

#include <cstdint>
#include <exception>

#include "h3ddata_schema_generated.h"
using namespace std;

namespace h3d_tools {
pybind11::array_t<GammaEvent> unpack_gamma_events(char const* const buf) {
  if (buf == nullptr) {
    throw std::invalid_argument(
        "h3d_tools_cpp::unpack_gamma_events(): "
        "Provided buffer is NULL");
  }
  auto packet = H3DData::GetH3DPacket(buf);
  auto events = packet->gammaevents();
  auto const N = events->size();
  // Figure out how many total interactions
  size_t nint = 0;
  for (size_t i = 0; i < N; ++i) {
    nint += events->Get(i)->interactions()->size();
  }

  // Copy the data
  pybind11::array_t<GammaEvent> output(nint);
  auto optr = static_cast<GammaEvent*>(output.request().ptr);
  size_t itotal = 0;
  for (size_t iev = 0; iev < events->size(); ++iev) {
    auto event = events->Get(iev);
    auto gammas = event->interactions();
    for (size_t ig = 0; ig < gammas->size(); ++ig) {
      GammaEvent ge = {
          gammas->Get(ig)->chip(),
          // According to the API docs, this is a different
          // coordinate system than the global one. The relationship:
          // X global = -Y listmode
          // Y global = +Z listmode
          // Z global = -X listmode
          // where (X:Down, Y:Right, Z:Forward)
          // Convert to right-handed coordinates by negating X
          // thus (X:Up, Y:Right, Z:Forward)
          // Units are 10s of microns, so convert to meters
          gammas->Get(ig)->x() * -1e-5f, gammas->Get(ig)->y() * 1e-5f,
          gammas->Get(ig)->z() * 1e-5f,
          // Convert from eV to keV
          gammas->Get(ig)->energy() * 1e-3f,
          // No conversion [10s of ns], but =0 for interactions beyond the first
          (ig == 0 ? (float)event->livetime() : 0.f),
          // No unit conversion - leave as FPGA timestamp and convert later
          (double)event->timestamp(), (uint16_t)gammas->size()};
      optr[itotal] = ge;
      ++itotal;
    }
  }
  return output;
}

pybind11::array_t<ClockEvent> unpack_clock_events(char const* const buf) {
  if (buf == nullptr) {
    throw std::invalid_argument(
        "h3d_tools_cpp::unpack_clock_events(): "
        "Provided buffer is NULL");
  }
  auto packet = H3DData::GetH3DPacket(buf);
  auto events = packet->clockevents();
  auto const N = events->size();
  pybind11::array_t<ClockEvent> output(N);
  auto optr = static_cast<ClockEvent*>(output.request().ptr);
  for (size_t i = 0; i < N; ++i) {
    ClockEvent ce = {
        events->Get(i)->clockseconds(), events->Get(i)->clocknanoseconds(),
        // No unit conversion - leave as FPGA timestamp and convert later
        (double)events->Get(i)->timestamp()};
    optr[i] = ce;
  }
  return output;
}

pybind11::array_t<MaskEvent> unpack_mask_events(char const* const buf) {
  if (buf == nullptr) {
    throw std::invalid_argument(
        "h3d_tools_cpp::unpack_mask_events(): "
        "Provided buffer is NULL");
  }
  auto packet = H3DData::GetH3DPacket(buf);
  auto events = packet->maskevents();
  auto const N = events->size();
  pybind11::array_t<MaskEvent> output(N);
  auto optr = static_cast<MaskEvent*>(output.request().ptr);
  for (size_t i = 0; i < N; ++i) {
    MaskEvent me = {
        events->Get(i)->rotating(), events->Get(i)->quadrant(),
        // No conversion [msec] for RT and LT
        (float)events->Get(i)->realtime(), (float)events->Get(i)->livetime(),
        // No unit conversion - leave as FPGA timestamp and convert later
        (double)events->Get(i)->timestamp()};
    optr[i] = me;
  }
  return output;
}

pybind11::array_t<SyncEvent> unpack_sync_events(char const* const buf) {
  if (buf == nullptr) {
    throw std::invalid_argument(
        "h3d_tools_cpp::unpack_sync_events(): "
        "Provided buffer is NULL");
  }
  auto packet = H3DData::GetH3DPacket(buf);
  auto events = packet->syncevents();
  auto const N = events->size();
  pybind11::array_t<SyncEvent> output(N);
  auto optr = static_cast<SyncEvent*>(output.request().ptr);
  for (size_t i = 0; i < N; ++i) {
    SyncEvent se = {
        events->Get(i)->index(),
        // No unit conversion - leave as FPGA timestamp and convert later
        (double)events->Get(i)->timestamp()};
    optr[i] = se;
  }
  return output;
}
}  // namespace h3d_tools
