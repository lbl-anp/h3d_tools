#ifndef FLATPARSER_HPP
#define FLATPARSER_HPP
#include <pybind11/numpy.h>

namespace h3d_tools {
/* These structures are essentially the same as what flatbuffers uses, except
 * we will use these to create the structured numpy.ndarrays via the python
 * bindings. Coordinate rotations and desired unit conversions are already done
 * by the time the data hits these objects.
 *
 * These attributes (along with their types) are used for the numpy.ndarray
 * generation. The results are numpy structured arrays with the struct field
 * names and types from the structs defined below.
 *
 * The units noted are the units *after* parsing. See the H3D flatbuffers
 * schema to see the units/types coming from the raw byte stream (via
 * flatbuffers).
 */

/*! \struct GammaEvent
 * H3D refers to these data as an interaction. GammaEvent contains a logical
 * gamma- ray interaction in the detector volume.
 *
 * For GammaEvents with num_interaction > 1, the first GammaEvent will contain
 * the livetime, and the subsequent interactions will have livetime=0. This
 * allows one to np.sum(data["lt"]) to get the total livetime.
 *
 * Similarly, for num_interaction > 1, the subsequent interactions will have the
 * same FPGA timestamp as the first.
 */
struct GammaEvent {
  /// Crystal index
  std::uint8_t det_id;
  /// X position [m]
  float x;
  /// Y position [m]
  float y;
  /// Z position [m]
  float z;
  /// Energy [keV]
  float e;
  /// Live time [10s of ns]
  float lt;
  /// FPGA timestamp [10s of ns]
  double ts;
  /// Number of interactions
  std::uint16_t num_interaction;
};
/*! \struct ClockEvent
 * The H3D systems periodically send the local system clock time, along with the
 * associated FPGA time, which can be used to interpolate GammaEvent timestamps
 * into system (epoch) time.
 */
struct ClockEvent {
  /// System epoch time seconds [sec]
  std::uint32_t sys_sec;
  /// System epoch time nanoseconds since last second [ns]
  std::uint32_t sys_ns;
  /// FPGA timestamp [10s of ns]
  double ts;
};
/*! \struct MaskEvent
 * Information about the state of the coded mask, for H3D systems that have one,
 * such as the H420.
 */
struct MaskEvent {
  /// Is the mask rotating?
  bool rotating;
  /// Which quadrant [0,1,2,3]?
  std::uint8_t quadrant;
  /// Real time [ms]
  float rt;
  /// Live time [ms]
  float lt;
  /// FPGA timestamp [10s of ns]
  double ts;
};
/*! \struct SyncEvent
 * Used for synchronizing multiple H3D detector systems working together. Not
 * explicitly tested since LBNL has not needed this functionality.
 */
struct SyncEvent {
  /// Index
  std::uint64_t index;
  /// FPGA timestamp [10s of ns]
  double ts;
};

/* Unpacker functions. Given the raw byte buffer from python, these will
 * return structured numpy arrays with the data. */
/*! \fn
 * Unpack GammaEvent data using flatbuffers
 * \param buf A `bytes` object from python
 * \return A numpy.ndarray structured array with fields: det_id, x, y, z, e,
 *         lt, ts, num_interaction
 */
pybind11::array_t<GammaEvent> unpack_gamma_events(char const* const buf);
/*! \fn
 * Unpack ClockEvent data using flatbuffers
 * \param buf A `bytes` object from python
 *  \return A numpy.ndarray structured array with fields: sys_sec, sys_ns,
 *          ts
 */
pybind11::array_t<ClockEvent> unpack_clock_events(char const* const buf);
/*! \fn
 *  Unpack MaskEvent data using flatbuffers
 *  \param buf A `bytes` object from python
 *  \return A numpy.ndarray structured array with fields: rotating, quadrant,
 *          rt, lt, ts
 */
pybind11::array_t<MaskEvent> unpack_mask_events(char const* const buf);
/*! \fn
 *  Unpack SyncEvent data using flatbuffers
 *  \param buf A `bytes` object from python
 *  \return A numpy.ndarray structured array with fields: index, ts
 */
pybind11::array_t<SyncEvent> unpack_sync_events(char const* const buf);
}  // namespace h3d_tools

#endif
