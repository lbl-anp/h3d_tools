import requests
from pathlib import Path

# Provided by Willy from H3D; these are arbitrary, delta-temperature units
TEMP_OK_LIMIT = 4
TEMP_WARNING_LIMIT = 8
HUMIDITY_OK_LIMIT = 30.0
HUMIDITY_WARNING_LIMIT = 50.0


class H3DStatus(object):
    """Status communication interface for H3D detector systems. Currently
    only tested with the Polaris H420.

    Use this class to send commands or requests to the detector, such as
    "start measurement", "stop measurement", or "get optical image". This is
    communicated using HTTP, mostly POST.
    """

    def __init__(
        self,
        detector_ip: str = "192.168.2.10",
        web_timeout: int = 3,  # seconds
        verbose: bool = False,
    ):
        """Initialize web connection info.

        Parameters
        ----------
        detector_ip : str
            IP address of the detector to communicate with over HTTP
        web_timeout : int
            Number of seconds to wait before timing out on an HTTP request
        verbose : bool
            Toggle verbosity
        """
        self.verbose = verbose
        if self.verbose:
            print("H3DStatus: initializing")
        self.detector_ip = detector_ip
        self.web_url = f"http://{detector_ip}"
        self.web_timeout = web_timeout

    def __str__(self):
        """A quick printout of the H3DStatus parameters.

        Returns
        -------
        str
            Parameters of this H3DStatus object
        """
        my_str = f"detector_ips: {self.detector_ip}\n"
        my_str += f"web_url: {self.web_url}\n"
        my_str += f"web_timeout: {self.web_timeout} sec\n"
        return my_str

    def start_acq(self, acq_name="TestAcq"):
        """Start a measurement. Checks first if there is one already running,
        to avoid repeatedly engaging the mask motor.

        Parameters
        ----------
        acq_name : str
            Name of the measurement, used by the H3D detectors. It's wise to avoid
            special characters here!
        """
        # Check if the detector is already acquiring, to avoid possibly sending
        # repeated commands, which will engage the mask motor (yikes!)
        if self.is_acquiring():
            if self.verbose:
                print("Detector is already acquiring, command ignored. STOP first!")
            return
        if self.verbose:
            print("H3DStatus: sending START command")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/start",
                json={"name": acq_name},
                timeout=self.web_timeout,
            )
            resp.raise_for_status()
        except Exception as ex:
            print(f"H3DStatus.start_acq(): {ex}")

    def stop_acq(self):
        """Stop a measurement."""
        if not self.is_acquiring():
            if self.verbose:
                print("Detector is not acquiring, command ignored. START first!")
            return
        if self.verbose:
            print("H3DStatus: sending STOP command")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/stop", timeout=self.web_timeout
            )
            resp.raise_for_status()
        except Exception as ex:
            print(f"H3DStatus.stop_acq(): {ex}")

    def set_fixed_acq(self, enable=True, duration=60):
        """Toggle whether or not to have fixed duration runs, and set the
        duration (in seconds) Does NOT start an acquisition (use start_acq()).
        Setting to False will result in measurements running until stop_acq()
        is called.

        Parameters
        ----------
        enable : bool
            Toggle the fixed acquisition on (True) or off (False)
        duration : int
            Duration of time in seconds for fixed measurements
        """
        if self.verbose:
            print("H3DStatus: toggling fixed run duration")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/auto-stop",
                json={"enable": enable, "length": duration},
                timeout=self.web_timeout,
            )
            resp.raise_for_status()
        except Exception as ex:
            print(f"H3DStatus.set_auto_acq(): {ex}")

    def get_spectrum(self) -> str:
        """Request the current energy spectrum.

        Returns
        -------
        str
            Returns a newline-separated string of integer energy bin counts.
        """
        if self.verbose:
            print("H3DStatus: asking for an energy spectrum")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/spectrum",
                json={"format": "txt"},
                timeout=self.web_timeout,
            )
            resp.raise_for_status()
            # All of the newlines are escaped, so remove the escapes
            # TODO: just return a list of numbers
            return resp.content.decode("unicode_escape")
        except Exception as ex:
            print(f"H3DStatus.get_spectrum(): {ex}")
            return ""

    def get_temp_status(self) -> str:
        """Get the difference between the current detector temperature, and the
        target temperature. According to H3D, the units are arbitrary so as to
        obfuscate proprietary info. The ranges are defined as follows:
            0 <= abs(t) < 4 : OK
            4 <= abs(t) < 8 : WARNING (should still "work")
            abs(t) >= 8     : ERROR (concern about peaks drifting)

        Returns
        -------
        str
            One of four string statuses: 'OK', 'WARNING', 'ERROR (<temp value>),
            or 'UNAVAILABLE' if communication failed.
        """
        if self.verbose:
            print("H3DStatus: asking for the temperature status")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/status/slow", timeout=self.web_timeout
            )
            resp.raise_for_status()
            temp = resp.json()["fCurrentTemperature"]
            if abs(temp) < TEMP_OK_LIMIT:
                return "OK"
            elif abs(temp) < TEMP_WARNING_LIMIT:
                return "WARNING"
            else:
                return f"ERROR ({temp})"
        except Exception as ex:
            print(f"H3DStatus.get_temp_status(): {ex}")
            return "UNAVAILABLE"

    def get_humidity_status(self) -> str:
        """Get the difference between the current detector relative humidity,
        and the target value. According to H3D, the units are % relative
        humidity. The ranges are defined as follows:
            0%  <= abs(rh) < 30% : OK
            30% <= abs(rh) < 50% : WARNING (should still "work")
            abs(t) >= 50%        : ERROR (concerns about long-term damage)

        Returns
        -------
        str
            One of four string statuses: 'OK', 'WARNING', 'ERROR (<RH delta %>),
            or 'UNAVAILABLE' if communication failed.
        """
        if self.verbose:
            print("H3DStatus: asking for the relative humidity status")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/status/fast", timeout=self.web_timeout
            )
            resp.raise_for_status()
            # % relative humidity DELTA from target value
            relhum = resp.json()["fRelativeHumidity"]
            if abs(relhum) < HUMIDITY_OK_LIMIT:
                return "OK"
            elif abs(relhum) < HUMIDITY_WARNING_LIMIT:
                return "WARNING"
            else:
                return f"ERROR ({relhum})"
        except Exception as ex:
            print(f"H3DStatus.get_humidity_status(): {ex}")
            return "UNAVAILABLE"

    def is_biased(self) -> bool:
        """Flag for whether or not the detector is currently biasing up.

        Returns
        -------
        bool
            True if the detector is biased, and False if it's ramping its bias up
        """
        if self.verbose:
            print("H3DStatus: asking if biasing")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/status/fast", timeout=self.web_timeout
            )
            resp.raise_for_status()
            if resp.json()["nBiasStatus"] == 0:
                return True
            else:
                return False
        except Exception as ex:
            print(f"H3DStatus.get_bias(): {ex}")
            return False

    def toggle_spatial_energy_mode(self):
        """This toggles between "high energy mode" and "spatial mode." In
        spatial mode, pixels are subdivided to obtain better spatial resolution,
        at the cost of higher energies (not sure if it just degrades or totally
        cuts off). For energies above 1.6 MeV, "high energy mode" is required.

        A bit confusingly, this flag is termed "bLowEnergyCal," which when
        True runs in spatial mode, and when False runs in high energy mode.

        Also note that this setting is persistent across system reboots.
        """
        if self.verbose:
            print("H3DStatus: attempting to set spatial/high energy mode toggle")
        # this needs to be tested with hardware first
        raise NotImplementedError
        # Check if it's already set
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/status/fast", timeout=self.web_timeout
            )
            resp.raise_for_status()
            # TODO: check the bLowEnergyCal property (needs testing), return
            # if it matches "enable"
            if resp.json()["bLowEnergyCal"] == "enable":
                print("Spatial mode is already enabled")
                return
            # else:
            # TODO: flip the toggle...they send a string argument "{}" with
            # the POST request as pure bytes, need to figure that out
            try:
                resp = requests.post(
                    f"{self.web_url}/api/v1/calibration",
                    # data="{}"?
                    timeout=self.web_timeout,
                )
                resp.raise_for_status()
            except Exception as ex:
                print(f"H3DStatus.set_low_energy_cal(): {ex}")
        except Exception as ex:
            print(f"H3DStatus.set_low_energy_cal(): {ex}")
        # TODO: H3D double checks that the value was set here by checking
        # bLowEnergyCal again but we'll see if that's necessary

    def get_optical_image(self) -> bytes:
        """Get a .jpg of the optical image.

        Returns
        -------
        bytes
            Raw bytes of the image
        """
        if self.verbose:
            print("H3DStatus: getting optical image")
        try:
            resp = requests.post(
                f"{self.web_url}/hires.jpg",
                timeout=self.web_timeout,
            )
            resp.raise_for_status()
            return resp.content
        except Exception as ex:
            print(f"H3DStatus.get_optical_image(): {ex}")
            return None

    def get_compton_image(self) -> bytes:
        """Get a .png of the Compton image.

        Returns
        -------
        bytes
            Raw bytes of the image
        """
        if self.verbose:
            print("H3DStatus: getting compton image")
        try:
            resp = requests.post(
                f"{self.web_url}/comptonimage.png", timeout=self.web_timeout
            )
            resp.raise_for_status()
            return resp.content
        except Exception as ex:
            print(f"H3DStatus.get_compton_image(): {ex}")
            return None

    def get_coded_image(self) -> bytes:
        """Get a .png of the coded-aperture image.

        Returns
        -------
        bytes
            Raw bytes of the image
        """
        if self.verbose:
            print("H3DStatus: getting coded-aperture image")
        try:
            resp = requests.post(
                f"{self.web_url}/caimage.png", timeout=self.web_timeout
            )
            resp.raise_for_status()
            return resp.content
        except Exception as ex:
            print(f"H3DStatus.get_coded_image(): {ex}")
            return None

    def get_bef(self) -> bytes:
        """Get the .bef file data.

        Returns
        -------
        bytes
            Raw bytes of the .bef file
        """
        return self.get_bef_bss("bef")

    def get_bss(self) -> bytes:
        """Get the .bss file data.

        Returns
        -------
        bytes
            Raw bytes of the .bss file
        """
        return self.get_bef_bss("bss")

    def get_bef_bss(self, extension: str) -> bytes:
        """Get the .bef or .bss file data. This downloads the file as
        binary and returns the data as a bytestring.

        Parameters
        ----------
        extension : str
            'bef' or 'bss', depending on which data is desired

        Returns
        -------
        bytes
            Raw bytes containing .bef or .bss file data
        """
        extension = extension.casefold()
        if extension not in ("bef", "bss"):
            raise ValueError(
                "H3DStatus.get_bef_bss(): invalid extension, must be bef or bss"
            )
        if self.verbose:
            print(f"H3DStatus: getting .{extension} data")
        try:
            # There's no API call, so we have to use basic HTTP requests
            # to first get the name of the directory where the files are
            # stored, then to get the file binary data
            resp = requests.get(
                f"{self.web_url}/scans/list-items?path=", timeout=self.web_timeout
            )
            resp.raise_for_status()
            # Get a list of all available stored measurements
            meas_names = []
            for r in resp.json()["items"]:
                meas_names.append(r["path"])
            # They are prefixed with YYYYMMDD-HHMMSS_<MeasurementName>
            # So sorting will put the most recent measurement last
            # <MeasurementName> = PL2Measurement (which we specify)
            meas_names = sorted(meas_names)
            if self.verbose:
                print(f"Getting .{extension} from measurement: {meas_names[-1]}")
            # Download the BEF data. Note that this call might take a few
            # seconds when it's successful
            # Also note this DOES NOT WORK WHILE A MEASUREMENT IS BEING
            # COLLECTED
            # It's because these files are actively being written to until
            # the measurement is finished!
            resp = requests.get(
                f"{self.web_url}/scans/download?path={meas_names[-1]}/"
                f"AllEvents.{extension}",
            )
            resp.raise_for_status()
            # If it fails, we'll get back an HTTP 200 code, which is not
            # helpful...it should return an error code!
            # Instead we have to check the bytestring that comes back,
            # which will be b'{"err":"Failed to download file(s)!"}'
            if "Failed" in str(resp.content):
                if self.verbose:
                    print(f"H3DStatus: Failed to get .{extension} for {meas_names[-1]}")
                return None
            else:
                return resp.content
        except Exception as ex:
            print(f"H3DStatus.get_bef(): {ex}")
            return None

    def is_acquiring(self) -> bool:
        """Check if the detector is currently taking a measurement.

        Returns
        -------
        bool
            True if a measurement is underway, False if not
        """
        if self.verbose:
            print("H3DStatus: checking acquisition state (acquiring or not?)")
        try:
            resp = requests.post(
                f"{self.web_url}/api/v1/status/fast", timeout=self.web_timeout
            )
            resp.raise_for_status()
            return resp.json()["nMeasurementMode"] != 0
        except Exception as ex:
            print(f"H3DStatus.is_acquiring(): {ex}")
            return False

    def write_h3d_data_products(self, path: str = "") -> None:
        """For the most recent measurement, download all of the H3D data
        products. These are stored in the same directory and are
        requestable via HTTP.

        This will skip nested (_SS) directories. Use
        write_h3d_data_products_zip() to get everything zipped into
        a single file.


        Parameters
        ----------
        path : str
            Path to the directory in which to write the data products. If
            not provided, will save to the current directory
        """
        if path == "":
            path = "."
        path.rstrip("/")
        if self.verbose:
            print(
                "H3DStatus: getting H3D data products. This may take some"
                " time, depending on the measurement time"
            )
        try:
            # There's no API call, so we have to use basic HTTP requests
            # to first get the name of the directory where the files are
            # stored, then to get the file binary data
            resp = requests.get(
                f"{self.web_url}/scans/list-items?path=", timeout=self.web_timeout
            )
            resp.raise_for_status()
            # Get a list of all available stored measurements
            meas_names = []
            for r in resp.json()["items"]:
                meas_names.append(r["path"])
            # They are prefixed with YYYYMMDD-HHMMSS_<MeasurementName>
            # So sorting will put the most recent measurement last
            # <MeasurementName> = PL2Measurement (which we specify)
            meas_names = sorted(meas_names)
            if self.verbose:
                print(f"Available measurement names: {meas_names}")

            # Get a list of all of the files (data products) in this measurement
            # directory
            resp = requests.get(
                f"{self.web_url}/scans/list-items?path={meas_names[-1]}",
                timeout=self.web_timeout,
            )
            resp.raise_for_status()
            meas_files = []
            for r in resp.json()["items"]:
                meas_files.append(r["path"])
            print(f"List of files to download: {meas_files}")
            # Download and write data products
            for hf in meas_files:
                # Skip nested directories, suffixed with _SS, which contain
                # ..."screenshots" from the camera?
                if not hf.contains("."):
                    continue
                print(f"Downloading {hf}")
                resp = requests.get(
                    f"{self.web_url}/scans/download?path={hf}",
                    # It's very slow
                    timeout=3600,
                )
                resp.raise_for_status()
                # If it fails, we'll get back an HTTP 200 code, which is not
                # helpful...it should return an error code!
                # Instead we have to check the bytestring that comes back,
                # which will be b'{"err":"Failed to download file(s)!"}'
                if "Failed" in str(resp.content):
                    print(f"H3DStatus: Failed to get {hf}")
                    print("NOTE: the measurement *must* be stopped first!!")
                    return
                p = Path(hf)
                print(f"Writing to {path}/{p.name}...")
                # There's a mix of files, .txt, .n42, .xml, and .bef/.bss (binary)
                if p.suffix in (".bef", ".bss", ".jpg"):
                    # Write raw binary
                    with open(f"{path}/{p.name}", "wb") as f:
                        f.write(resp.content)
                else:
                    # Decode to text, then write
                    with open(f"{path}/{p.name}", "w") as f:
                        f.write(resp.content.decode("utf-8"))

        except Exception as ex:
            print(f"H3DStatus.write_h3d_data_products(): {ex}")
            return

    def write_h3d_data_products_zip(self, path: str = ""):
        """For the most recent measurement, download all of the H3D data
        products, zipped up by the system. This is equivalent to clicking
        the 'download' link next to the directory name.

        Parameters
        ----------
        path : str
            Path to the directory in which to write the data products. If
            not provided, will save to the current directory
        """
        if path == "":
            path = "."
        path.rstrip("/")
        if self.verbose:
            print(
                "H3DStatus: getting zipped H3D data products. This may "
                "take some time, depending on the measurement time"
            )
        try:
            # There's no API call, so we have to use basic HTTP requests
            # to first get the name of the directory where the files are
            # stored, then to get the file binary data
            resp = requests.get(
                f"{self.web_url}/scans/list-items?path=", timeout=self.web_timeout
            )
            resp.raise_for_status()
            # Get a list of all available stored measurements
            meas_names = []
            for r in resp.json()["items"]:
                meas_names.append(r["path"])
            # They are prefixed with YYYYMMDD-HHMMSS_<MeasurementName>
            # So sorting will put the most recent measurement last
            # <MeasurementName> = PL2Measurement (which we specify)
            meas_names = sorted(meas_names)
            if self.verbose:
                print(f"Available measurement names: {meas_names}")

            # Download and write data products
            print(f"Downloading {meas_names[-1]}.zip")
            resp = requests.get(
                f"{self.web_url}/scans/download?path={meas_names[-1]}",
                # It's very slow
                timeout=3600,
            )
            resp.raise_for_status()
            # If it fails, we'll get back an HTTP 200 code, which is not
            # helpful...it should return an error code!
            # Instead we have to check the bytestring that comes back,
            # which will be b'{"err":"Failed to download file(s)!"}'
            if "Failed" in str(resp.content):
                print(f"H3DStatus: Failed to get {meas_names[-1]}.zip")
                print("NOTE: the measurement *must* be stopped first!!")
                return
            p = Path(f"{meas_names[-1]}.zip")
            print(f"Writing to {path}/{p.name}...")
            # Should just be a .zip file, write the binary
            with open(f"{path}/{p.name}", "wb") as f:
                f.write(resp.content)

        except Exception as ex:
            print(f"H3DStatus.write_h3d_data_products_zip(): {ex}")
            return
