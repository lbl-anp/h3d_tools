#!/usr/bin/env python
import socket
import select
import numpy as np
from typing import Tuple
from .h3dstatus import H3DStatus
from .h3d_tools_cpp import (
    unpack_gamma_events,
    unpack_clock_events,
    unpack_mask_events,
    unpack_sync_events,
)

# List of supported H3D detectors
H3D_SYSTEMS = ("h420", "m400")


class H3DDAQ(object):
    """Listmode communication interface for H3D detector systems. Currently
    only tested with the H3D H420 and M400.

    Use this class to read binary listmode data. H3D provides FlatBuffers
    structures to hold these data, however the python interface is too slow to be
    used at higher rates. Instead, we bind to C++ functions that do the binary
    unpacking and return numpy arrays to python.

    Note: Listmode data are always streaming as long as the detector is biased
    and has a USB drive inserted.

    This object has convenience functions for basic control, such as starting
    and stopping measurements. For detailed control or status information, use
    the H3DStatus object. This is done over HTTP - not over the listmode pipe.

    As requested by ORNL, there is the option to repeat the raw bytes on a
    specific port. This is a 1:1 reproduction of the raw data as it's
    received and before it's unpacked with flatbuffers.

    How to use:

    .. code-block:: python

        daq = H3DDAQ()
        daq.connect_listmode()
        while daq.listmode_connected():
            daq.receive_listmode()
            (g, c, m, s) = daq.get_event_data()
            # g is a structured array of gamma events, e.g., g["x"], g["e"], etc
            # c is a structured array of clock events (typically 0 or 1 events)

    0-length arrays may be returned if there is no data for that component, or
    if that component is not supported by a system (e.g. MaskEvents in the M400).
    """

    def __init__(
        self,
        detector_ip: str = "192.168.2.10",
        listmode_port: int = 11503,
        listmode_timeout: int = 2,  # seconds
        repeat_raw: bool = False,
        repeater_ip: str = "192.168.9.1",
        repeater_port: int = 4420,
        system_name: str = "h420",
        verbose: bool = False,
    ):
        """Create an H3DAQ object. There should only be 1 of these per detector in
        an application instance.

        Parameters
        ----------
        detector_ip : str
            IP address of the detector to connect to
        listmode_port : int
            Port used for listmode communication
        listmode_timeout : int
            Time in seconds to wait before timing out when reading the listmode
            socket
        repeat_raw : bool
            Toggle repeating/mirroring the listmode data on another socket
        repeater_ip : str
            IP address to repeat the listmode data on, if enabled
        repeater_port : str
            Port to repeat the listmode data on, if enabled
        system_name : str
            Model name of the H3D system. Must be in H3D_SYSTEMS
        """
        if verbose:
            print("H3DDAQ: initializing")
        self.detector_ip = detector_ip
        self.listmode_port = listmode_port
        # The listmode timeout effectively controls the maximum time that
        # receive_listmode() can block before deciding there are no data
        # Since the listmode packets constantly stream, this time *should*
        # always be small, e.g. < 1 sec
        self.listmode_timeout = listmode_timeout
        self.listmode_connected = False
        self.packet = None

        # Initialize as length-0 numpy arrays. For systems that don't support
        # a given component (e.g. MaskEvents on the M400), we will return a
        # 0-length array from get_event_data(), then the caller can check
        # the length (vs. checking against None)
        self.gamma_events = np.zeros(shape=(0,))
        self.clock_events = np.zeros(shape=(0,))
        self.mask_events = np.zeros(shape=(0,))
        self.sync_events = np.zeros(shape=(0,))

        # Flag for republishing the raw bytes on another socket
        self.repeat_raw = repeat_raw
        self.repeater_ip = repeater_ip
        self.repeater_port = repeater_port
        if self.repeat_raw:
            print(f"Repeating listmode data on {self.repeater_ip}:{self.repeater_port}")
        self.repeater_clients = []
        self.repeater = None

        # Check that this is a supported system
        if system_name.casefold() not in H3D_SYSTEMS:
            raise ValueError(f"Invalid system name provided: {system_name}")

        self.system_name = system_name
        # True -> print status, False -> print only errors
        self.verbose = verbose
        self.status = H3DStatus(detector_ip=self.detector_ip, verbose=self.verbose)

    def __str__(self) -> str:
        """A quick printout of the H3DDAQ parameters.

        Returns
        -------
        str
            String with the parameters for this object and its current data
        """
        my_str = f"system_name: {self.system_name}\n"
        my_str += f"verbose: {self.verbose}"
        my_str += f"detector_ips: {self.detector_ip}\n"
        my_str += f"listmode_port: {self.listmode_port}\n"
        my_str += f"listmode_timeout: {self.listmode_client.gettimeout()} sec\n"
        my_str += f"listmode_connected: {self.listmode_connected}\n"
        my_str += f"repeat_raw: {self.repeat_raw}\n"
        my_str += f"repeater_ip: {self.repeater_ip}\n"
        my_str += f"repeater_port: {self.repeater_port}\n"
        my_str += f"gamma event data: {self.gamma_events}"
        my_str += f"clock event data: {self.clock_events}"
        my_str += f"mask event data: {self.mask_events}"
        my_str += f"sync event data: {self.sync_events}"
        return my_str

    def connect_listmode(self) -> bool:
        """Connect to the listmode data stream.

        Returns
        -------
        bool
            True if connected successfully, False if not.
        """
        print(
            f"H3DDAQ: attempting to connect to {self.detector_ip} on "
            f"port {self.listmode_port} (listmode)"
        )
        try:
            self.listmode_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.listmode_client.settimeout(self.listmode_timeout)
            self.listmode_client.connect((self.detector_ip, self.listmode_port))
        except OSError as ex:
            print(f"H3DDAQ.connect_listmode(): {ex}")
            self.listmode_connected = False
            return False

        if self.repeat_raw:
            self.repeater = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.repeater.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.repeater.setblocking(False)
            self.repeater.bind((self.repeater_ip, self.repeater_port))
            self.repeater.listen()
            print(
                f"H3DDAQ: repeating listmode data on server {self.repeater_ip}:"
                f"{self.repeater_port}"
            )

        self.listmode_connected = True
        self.packet = None
        return True

    def start_acq(self, acq_name="TestAcq"):
        """Pass-through: calls H3DStatus.start_acq()."""
        self.status.start_acq(acq_name)

    def stop_acq(self):
        """Pass-through: calls H3DStatus.stop_acq()."""
        self.status.stop_acq()

    def set_fixed_acq(self, enable=True, duration=60):
        """Pass-through: calls H3DStatus.set_fixed_acq()."""
        self.status.set_fixed_acq(enable, duration)

    def is_acquiring(self) -> bool:
        """Pass-through: calls H3DStatus.is_acquiring()."""
        return self.status.is_acquiring()

    def receive_listmode(self) -> bool:
        """Grab the entire current buffer of listmode data. If enabled, the data
        is repeated to connections on the repeater socket.

        Returns
        -------
        bool
            True if a packet was received; returns False otherwise.
        """
        if self.verbose:
            print("H3DDAQ: grabbing listmode packet")
        if not self.listmode_connected:
            print(
                "ERROR: not connected to listmode data stream, use .connect_listmode()"
            )
            return False

        # Check for new clients on repeater socket
        if self.repeat_raw:
            try:
                conn, addr = self.repeater.accept()
                self.repeater_clients.append(conn)
                if self.verbose:
                    print(f"New client connected, id={conn}, addr={addr}")
            except BlockingIOError:
                # There were no pending connections
                pass

        to_read = [self.listmode_client]
        try:
            read_ready = select.select(
                to_read, [], [], self.listmode_client.gettimeout()
            )
        except OSError:
            print(
                "ERROR: Received OSError from 'select.select' trying to check for read"
                " ready."
            )
            return False
        packet_size = 0
        self.buffer = b""
        self.repeat_buf = b""
        if read_ready:
            try:
                psize_bytes = self.listmode_client.recv(4)
                self.repeat_buf += psize_bytes
                packet_size = int.from_bytes(
                    psize_bytes, byteorder="little", signed=True
                )
                # Packets may be split across sends; loop until we get the
                # whole thing
                while len(self.buffer) < packet_size:
                    self.buffer += self.listmode_client.recv(
                        packet_size - len(self.buffer)
                    )
                # self.repeat_buf has that 4 byte header, whereas self.buffer
                # does not!
                self.repeat_buf += self.buffer
                # Repeat if desired
                if self.repeat_raw:
                    for conn in self.repeater_clients:
                        try:
                            conn.sendall(self.repeat_buf)
                        except OSError:
                            # Client disconnected
                            self.repeater_clients.remove(conn)
                            if self.verbose:
                                print(f"Client {conn} disconnected")
            except socket.timeout:
                # Timeouts don't necessarily indicate a bad connection
                print("H3DDAQ.receive_listmode(): Timed out")
                return False
            except OSError as ex:
                print(f"H3DDAQ.receive_listmode(): {ex}")
                self.disconnect_listmode()
                return False
        else:
            if self.verbose:
                print("H3DDAQ.receive_listmode(): there were no data")
            return False

        if packet_size > 0:
            # Integrity check; this should always be true! If it happens,
            # though, try to skip to the next one
            if packet_size != len(self.buffer):
                print(
                    "H3DDAQ.receive_listmode(): ERROR! expected "
                    f"packet_size({packet_size}) != "
                    f"received buffer_size({len(self.buffer)})!\nSkipping packet"
                )
                return False
            if self.verbose:
                print(f"H3DDAQ.receive_listmode(): {packet_size} bytes received")
            return True
        else:
            # recv() returned 0 while read_ready was true, we're in a bad state
            self.disconnect_listmode()
            print("H3DDAQ receive_listmode(): packet size was 0")
            return False

    def get_event_data(
        self,
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Unpack a packet of event data.

        Returns
        -------
        tuple
            A tuple of numpy structured arrays of with event data. In order:
            (gamma, clock, mask, sync)
        """
        self.gamma_events = unpack_gamma_events(self.buffer)
        self.clock_events = unpack_clock_events(self.buffer)
        self.mask_events = unpack_mask_events(self.buffer)
        self.sync_events = unpack_sync_events(self.buffer)
        return (
            self.gamma_events,
            self.clock_events,
            self.mask_events,
            self.sync_events,
        )

    def disconnect_listmode(self):
        """Close the listmode data stream."""
        if not self.listmode_connected:
            return
        try:
            self.listmode_client.close()
            if self.repeat_raw:
                for conn in self.repeater_clients:
                    conn.close()
                self.repeater.close()
        except OSError as ex:
            print(f"H3DDAQ.disconnect_listmode(): {ex}")
        self.listmode_connected = False

    def get_last_buffer(self) -> bytes:
        """Get the raw bytes from the last packet/buffer read.

        Returns
        -------
        bytes
            The raw byte data for the last packet that was received
        """
        return self.repeat_buf
