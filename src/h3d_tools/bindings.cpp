#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>

#include "flatparser.hpp"
namespace py = pybind11;

PYBIND11_MODULE(h3d_tools_cpp, m) {
  m.doc() = "h3d_tools python-bound C++ submodule";

  // Makes it so we can return a pybind11::array_t<GammaEvent> and it comes
  // out as a numpy.ndarray with these fields (and their respective types)
  PYBIND11_NUMPY_DTYPE(h3d_tools::GammaEvent, det_id, x, y, z, e, lt, ts,
                       num_interaction);
  PYBIND11_NUMPY_DTYPE(h3d_tools::ClockEvent, sys_sec, sys_ns, ts);
  PYBIND11_NUMPY_DTYPE(h3d_tools::MaskEvent, rotating, quadrant, rt, lt, ts);
  PYBIND11_NUMPY_DTYPE(h3d_tools::SyncEvent, index, ts);

  // Define the GammaEvent structure - readonly since we're just using for
  // storage
  py::class_<h3d_tools::GammaEvent> gamma_event(m, "GammaEvent");
  gamma_event.def(py::init<>())
      .def_readonly("det_id", &h3d_tools::GammaEvent::det_id)
      .def_readonly("x", &h3d_tools::GammaEvent::x)
      .def_readonly("y", &h3d_tools::GammaEvent::y)
      .def_readonly("z", &h3d_tools::GammaEvent::z)
      .def_readonly("e", &h3d_tools::GammaEvent::e)
      .def_readonly("lt", &h3d_tools::GammaEvent::lt)
      .def_readonly("ts", &h3d_tools::GammaEvent::ts)
      .def_readonly("num_interaction", &h3d_tools::GammaEvent::num_interaction);
  // ClockEvent
  py::class_<h3d_tools::ClockEvent> clock_event(m, "ClockEvent");
  clock_event.def(py::init<>())
      .def_readonly("sys_sec", &h3d_tools::ClockEvent::sys_sec)
      .def_readonly("sys_ns", &h3d_tools::ClockEvent::sys_ns)
      .def_readonly("ts", &h3d_tools::ClockEvent::ts);
  // MaskEvent
  py::class_<h3d_tools::MaskEvent> mask_event(m, "MaskEvent");
  mask_event.def(py::init<>())
      .def_readonly("rotating", &h3d_tools::MaskEvent::rotating)
      .def_readonly("quadrant", &h3d_tools::MaskEvent::quadrant)
      .def_readonly("rt", &h3d_tools::MaskEvent::rt)
      .def_readonly("lt", &h3d_tools::MaskEvent::lt)
      .def_readonly("ts", &h3d_tools::MaskEvent::ts);
  // SyncEvent
  py::class_<h3d_tools::SyncEvent> sync_event(m, "SyncEvent");
  sync_event.def(py::init<>())
      .def_readonly("index", &h3d_tools::SyncEvent::index)
      .def_readonly("ts", &h3d_tools::SyncEvent::ts);

  // Parser functions
  m.def("unpack_gamma_events", &h3d_tools::unpack_gamma_events);
  m.def("unpack_clock_events", &h3d_tools::unpack_clock_events);
  m.def("unpack_mask_events", &h3d_tools::unpack_mask_events);
  m.def("unpack_sync_events", &h3d_tools::unpack_sync_events);
}
